#!/usr/bin/env bash

dnf_config() {
  # Optimize DNF
  sudo sh -c 'cat <<EOF >> /etc/dnf/dnf.conf

# Speed Optimizations
fastestmirror=True
max_parallel_downloads=5
keepcache=True

# QOL
defaultyes=True
EOF'

  # Update System
  sudo dnf update

  # Install packages
  # shellcheck disable=SC2046
  sudo dnf install -y $(xargs < pkglist.dnf)
}

ssh() {
  sudo sh -c 'cat <<EOF >> /etc/ssh/sshd_config.d/secure.conf
Port 10222
PermitRootLogin no
PasswordAuthentication no
PermitEmptyPasswords no
EOF'

  sudo semanage port -a -t ssh_port_t -p tcp 10222
  sudo firewall-cmd --permanent --service="ssh" --add-port "10222/tcp"
  sudo firewall-cmd --permanent --service="ssh" --remove-port "22/tcp"
  sudo firewall-cmd --reload

  sudo systemctl restart sshd.service
}

passwordless_sudo() {
  sudo sh -c 'cat <<EOF >> /etc/sudoers.d/passwordless
%wheel ALL=(ALL) NOPASSWD: ALL
EOF'
}

rust() {
  # Rustup
  rustup-init -y --default-host x86_64-unknown-linux-gnu --default-toolchain stable --profile default

  # shellcheck disable=SC1091
  source "${HOME}/.cargo/env"

  rustup update

  # Install cargo packages
  # shellcheck disable=SC2046
  cargo install $(xargs < pkglist.cargo)
}

laptop() {
  # If laptop, disable lid switch
  sudo sh -c 'cat << EOF > /etc/systemd/logind.conf
[Login]
HandleLidSwitch=ignore
HandleLidSwitchExternalPower=ignore
HandleLidSwitchDocked=ignore
EOF'

  sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target

  sudo systemctl restart systemd-logind
}

nerd_font() {
  # Install nerd font
  git clone --filter=blob:none --sparse https://github.com/ryanoasis/nerd-fonts.git /tmp/nerd-fonts

  cd /tmp/nerd-fonts || sh -c 'echo "Failed to cd to: /tmp/nerd-fonts"; exit 1'

  git sparse-checkout add patched-fonts/SourceCodePro

  ./install.sh SourceCodePro

  cd - || sh -c 'echo "Failed to return to CWD"; exit 1'

  rm -rf /tmp/nerd-fonts
}

usage() {
  cat <<EOF >&2
Usage: $0
    -d  Disable dnf configuration
    -f  Do not install fonts
    -h  Print usage
    -l  Enable laptop specific configurations
    -p  Disable passwordless sudo
    -r  Disable rust support
    -s  Disable ssh configuration
EOF
  exit 1
}

OPTSTRING=":dfhlprs"

while getopts "${OPTSTRING}" OPTION; do
  case "${OPTION}" in
    d)
      NO_DNF="true"      
      ;;
    f)
      NO_FONTS="true"
      ;;
    h)
      usage
      ;;
    l)
      LAPTOP="true"
      ;;
    p)
      NO_PASSWORDLESS_SUDO="true"
      ;;
    r)
      NO_RUST="true"
      ;;
    s)
      NO_SSH="true"
      ;;
    ?)
      usage
      ;;
  esac
done

if [ -z "${NO_DNF}" ]; then
  dnf_config
else 
  YELLOW='\033[1;33m'
  NC='\033[0m'
  echo -e "${YELLOW}WARNING${NC}: Without dnf configuration other steps may fail." >&2
fi

if [ -z "${NO_SSH}" ]; then
  ssh
fi

if [ -z "${NO_PASSWORDLESS_SUDO}" ]; then
  passwordless_sudo
fi

if [ -z "${NO_RUST}" ]; then
  rust
fi

if [ -n "${LAPTOP}" ]; then
  laptop
fi

if [ -z "${NO_FONTS}" ]; then
  nerd_font
fi
